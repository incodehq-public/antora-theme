function incode_block_macro (opts) {
  if (opts.verb === 'jira') {
    return 'link:https://incodehq.atlassian.net/browse/' + opts.id + '[' + opts.id + ']'
  }
}

module.exports = function (registry) {
  registry.blockMacro(function () {
    var self = this
    self.named('incode')
    self.process(function (parent, target, attrs) {
      var id = attrs.id
      var result = incode_block_macro({ verb: target, id: id })
      return self.createBlock(parent, 'paragraph', result)
    })
  })
}
